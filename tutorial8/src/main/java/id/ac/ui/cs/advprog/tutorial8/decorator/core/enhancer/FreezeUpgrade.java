package id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

public class FreezeUpgrade extends Skill {

    Skill skill;

    public FreezeUpgrade(Skill skill) {
        this.skill= skill;
    }

    @Override
    public String getName() {
        return skill.getName();
    }

    @Override
    public int getSkillPower() {
        return skill.getSkillPower();
    }

    @Override
    public int getSkillManacost(){
        return skill.getSkillManacost();
    }

    // Adds freeze description to skill
    @Override
    public String getDescription() {
        return skill.getDescription() + " + freeze";
    }
}
