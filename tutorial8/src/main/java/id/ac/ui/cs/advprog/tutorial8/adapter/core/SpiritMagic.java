package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class SpiritMagic implements Magic {

    @Override
    public String cast() {
        return "summoning heroes of the past";
    }

    @Override
    public int getMagicCost() {
        return 120;
    }

    @Override
    public String description() {
        return "Spirit Magic";
    }
}
