package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class SwordMagic implements Magic {

    @Override
    public String cast() {
        return "sword strike, wait is this a magic?";
    }

    @Override
    public int getMagicCost() {
        return 20;
    }

    @Override
    public String description() {
        return "Sword (Magic)";
    }
}
