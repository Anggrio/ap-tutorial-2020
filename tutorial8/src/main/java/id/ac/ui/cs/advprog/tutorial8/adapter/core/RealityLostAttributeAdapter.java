package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class RealityLostAttributeAdapter implements Magic {

    private RealityLostAttribute realityLostAttribute;

    public RealityLostAttributeAdapter(RealityLostAttribute realityLostAttribute) {
        this.realityLostAttribute = realityLostAttribute;
    }

    @Override
    public String cast() {
        return "this ability is beyond human understanding";
    }

    @Override
    public int getMagicCost() {
        return Integer.parseInt(realityLostAttribute.attributeSacrifice());
    }

    @Override
    public String description() {
        return realityLostAttribute.attributeCalling();
    }
}
