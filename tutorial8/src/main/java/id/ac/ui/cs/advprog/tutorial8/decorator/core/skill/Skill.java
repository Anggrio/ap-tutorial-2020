package id.ac.ui.cs.advprog.tutorial8.decorator.core.skill;

public class Skill {

    protected String skillName;
    protected String skillDescription;
    protected int skillPower;
    protected int skillManacost;

    public String getName(){
        return skillName;
    }

    public int getSkillPower(){
        return skillPower;
    }

    public int getSkillManacost(){
        return skillManacost;
    }

    public String getDescription(){
        return skillDescription;
    }

}
