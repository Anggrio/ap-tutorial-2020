package id.ac.ui.cs.advprog.tutorial8.adapter.controller;

import id.ac.ui.cs.advprog.tutorial8.adapter.core.MagicResponseWrapper;
import id.ac.ui.cs.advprog.tutorial8.adapter.core.MagicianResponseWrapper;
import id.ac.ui.cs.advprog.tutorial8.adapter.service.MagicService;
import id.ac.ui.cs.advprog.tutorial8.adapter.service.MagicianService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class APIController {

    MagicianService magicianService = new MagicianService();
    MagicService magicService= new MagicService();

    @GetMapping("/magician")
    public MagicianResponseWrapper getMagician() {
        return magicianService.findMagician();
    }

    @PostMapping("/learn")
    public MagicianResponseWrapper learnMagic(@RequestBody MagicResponseWrapper wrapper) {
        magicianService.learnMagic(wrapper.getMagicName());
        return magicianService.findMagician() ;
    }

    @GetMapping("/magic")
    public List<MagicResponseWrapper> allMagic() {
        return magicService.findAll();
    }

    @PostMapping("/reset")
    public MagicianResponseWrapper resetMagician() {
        magicianService.resetMagician();
        return magicianService.findMagician();
    }
}
