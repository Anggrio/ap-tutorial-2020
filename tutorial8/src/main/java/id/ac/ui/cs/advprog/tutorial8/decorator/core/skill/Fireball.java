package id.ac.ui.cs.advprog.tutorial8.decorator.core.skill;

public class Fireball extends Skill {

        public Fireball() {
                skillName = "Fireball";
                skillDescription = "A ball of fire";
                skillPower = 250;
                skillManacost = 300;
        }
}
