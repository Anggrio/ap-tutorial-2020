package id.ac.ui.cs.advprog.tutorial8.decorator.service;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

public interface EnhanceService {

    void enhancePower();
    void enhanceManacost();
    void enhanceFreeze();
    void enhanceLifesteal();
    Iterable<Skill> getAllSkills();
}
