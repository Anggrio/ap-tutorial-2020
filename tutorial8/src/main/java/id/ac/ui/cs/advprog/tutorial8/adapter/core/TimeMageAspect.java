package id.ac.ui.cs.advprog.tutorial8.adapter.core;

import java.util.concurrent.ThreadLocalRandom;

public class TimeMageAspect {

    private static final String USE = "stopping time... 7 seconds has passed";
    private static final String NAME = "Time Mage Aspect";

    public String use() {
        return USE;
    }

    public String name() {
        return NAME;
    }

    public int magicRequirements() {
        return ThreadLocalRandom.current().nextInt(4500, 100000);
    }
}
