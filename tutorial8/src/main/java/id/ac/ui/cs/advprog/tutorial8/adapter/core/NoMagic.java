package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class NoMagic implements Magic {

    @Override
    public String cast() {
        return "imagination.. (rainbow)";
    }

    @Override
    public int getMagicCost() {
        return 0;
    }

    @Override
    public String description() {
        return "This is not a magic.";
    }
}
