package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class MagicianResponseWrapper {

    private String attack;
    private String learn;

    public String getAttack() {
        return attack;
    }

    public void setAttack(String attack) {
        this.attack = attack;
    }

    public String getLearn() {
        return learn;
    }

    public void setLearn(String learn) {
        this.learn = learn;
    }
}
