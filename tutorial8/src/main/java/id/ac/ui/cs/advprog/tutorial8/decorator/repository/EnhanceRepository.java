package id.ac.ui.cs.advprog.tutorial8.decorator.repository;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EnhanceRepository {

    public void enhancePower(List<Skill> skills) {
        for (Skill skill : skills) {
            Skill decoratedSkill;
            decoratedSkill = EnhancerDecorator.POWER_UPGRADE.addSkillEnhancement(skill);
            int index = skills.indexOf(skill);
            skills.set(index,decoratedSkill);
        }
    }

    public void enhanceManacost(List<Skill> skills) {
        for (Skill skill : skills) {
            Skill decoratedSkill;
            decoratedSkill = EnhancerDecorator.MANACOST_UPGRADE.addSkillEnhancement(skill);
            int index = skills.indexOf(skill);
            skills.set(index,decoratedSkill);
        }
    }

    public void enhanceFreeze(List<Skill> skills) {
        for (Skill skill : skills) {
            Skill decoratedSkill;
            decoratedSkill = EnhancerDecorator.FREEZE_UPGRADE.addSkillEnhancement(skill);
            int index = skills.indexOf(skill);
            skills.set(index,decoratedSkill);
        }
    }

    public void enhanceLifesteal(List<Skill> skills) {
        for (Skill skill : skills) {
            Skill decoratedSkill;
            decoratedSkill = EnhancerDecorator.LIFESTEAL_UPGRADE.addSkillEnhancement(skill);
            int index = skills.indexOf(skill);
            skills.set(index,decoratedSkill);
        }
    }
}
