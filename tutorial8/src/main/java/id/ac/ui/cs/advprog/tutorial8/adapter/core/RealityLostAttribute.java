package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class RealityLostAttribute {

    private static final String SACRIFICE = "999999";
    private static final String CALLING = "Reality Manipulation Lost Attribute";

    public void changeReality() {
        // This section is beyond human understanding
    }
    public String attributeSacrifice() {
        return SACRIFICE;
    }

    public String attributeCalling() {
        return CALLING;
    }
}
