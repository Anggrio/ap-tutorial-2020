package id.ac.ui.cs.advprog.tutorial8.decorator.core.skill;

public class Thunderbolt extends Skill {

        public Thunderbolt() {
                skillName = "Thunderbolt";
                skillDescription = "A bolt of thunder";
                skillPower = 200;
                skillManacost = 150;
        }
}
