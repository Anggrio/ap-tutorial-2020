package id.ac.ui.cs.advprog.tutorial8.decorator.controller;

import id.ac.ui.cs.advprog.tutorial8.decorator.service.EnhanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EnhanceController {

    private static final String REDIRECT_HOME = "redirect:/home";

    @Autowired
    EnhanceService enhanceService;

    @GetMapping("/home")
    public String home(Model model) {
        model.addAttribute("skills", enhanceService.getAllSkills());
        return "decorator/home";
    }

    @PostMapping("/enhance-power")
    public String enhancePower() {
        enhanceService.enhancePower();
        return REDIRECT_HOME;
    }

    @PostMapping("/enhance-manacost")
    public String enhanceManacost() {
        enhanceService.enhanceManacost();
        return REDIRECT_HOME;
    }

    @PostMapping("/enhance-freeze")
    public String enhanceFreeze() {
        enhanceService.enhanceFreeze();
        return REDIRECT_HOME;
    }

    @PostMapping("/enhance-lifesteal")
    public String enhanceLifesteal() {
        enhanceService.enhanceLifesteal();
        return REDIRECT_HOME;
    }
}
