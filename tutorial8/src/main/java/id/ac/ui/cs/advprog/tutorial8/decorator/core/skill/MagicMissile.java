package id.ac.ui.cs.advprog.tutorial8.decorator.core.skill;

public class MagicMissile extends Skill {

        public MagicMissile() {
                skillName = "Magic Missile";
                skillDescription = "A missile of magic";
                skillPower = 150;
                skillManacost = 200;
        }
}
