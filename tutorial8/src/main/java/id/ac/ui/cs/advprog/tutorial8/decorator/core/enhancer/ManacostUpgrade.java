package id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

public class ManacostUpgrade extends Skill {

    Skill skill;

    public ManacostUpgrade(Skill skill) {
        this.skill= skill;
    }

    @Override
    public String getName() {
        return skill.getName();
    }

    @Override
    public int getSkillPower() {
        return skill.getSkillPower();
    }

    // Reduces skill manacost
    @Override
    public int getSkillManacost() {
        if (skill.getSkillPower() == 0) {
            return 0;
        }
        return skill.getSkillPower() - 50;
    }

    @Override
    public String getDescription() {
        return skill.getDescription() + " + manacost reduction";
    }
}
