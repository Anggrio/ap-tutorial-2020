package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class TimeMageAspectAdapter implements Magic {

    private TimeMageAspect timeMageAspect;

    public TimeMageAspectAdapter(TimeMageAspect timeMageAspect) {
        this.timeMageAspect = timeMageAspect;
    }

    @Override
    public String cast() {
        return timeMageAspect.use();
    }

    @Override
    public int getMagicCost() {
        return timeMageAspect.magicRequirements();
    }

    @Override
    public String description() {
        return timeMageAspect.name();
    }
}
