package id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

public class PowerUpgrade extends Skill {

    Skill skill;

    public PowerUpgrade(Skill skill) {
        this.skill = skill;
    }

    @Override
    public String getName() {
        return skill.getName();
    }

    // Powers up skill power
    @Override
    public int getSkillPower() {
        return skill.getSkillPower() + 50;
    }

    @Override
    public int getSkillManacost(){
        return skill.getSkillManacost();
    }

    @Override
    public String getDescription() {
        return skill.getDescription() + " + powered up";
    }
}
