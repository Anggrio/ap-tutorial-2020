package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public interface Magic {

    String cast();
    int getMagicCost();
    String description();
}
