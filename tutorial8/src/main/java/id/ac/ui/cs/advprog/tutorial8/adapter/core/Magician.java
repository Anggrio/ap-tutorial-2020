package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class Magician {

    Magic magic;
    private String name;
    private int magicCapability;

    public Magician(String name, int magicCapability) {
        this.name = name;
        this.magicCapability = magicCapability;
    }

    public String learnMagic(Magic magic) {
        if (magic.getMagicCost() > magicCapability || magic instanceof NoMagic) {
            this.magic = null;
            return String.format("This magician, %s, can't learn such a powerful magic with this power level", this.name);
        }
        this.magic = magic;
        return String.format("This magician, %s had successfully learnt %s for %d years", this.name, magic.description(),
                getYearsForLearn(magic.getMagicCost()- this.magicCapability));
    }

    public String learnMagic() {
        if (this.magic==null) {
            return learnMagic(new NoMagic());
        } else return learnMagic(this.magic);
    }

    public String attack() {
        if (this.magic==null || this.magic instanceof NoMagic) {
            return "Attacking without magic for this magician, "+ this.name +", not yet knows any magic";
        }
        return String.format("Attacking with %s", this.magic.cast());
    }

    private int getYearsForLearn(int diff) {
        if (diff > 0) {
            return diff;
        } else {
            return 1;
        }
    }
}
