# Tutorial 8
"Seems like you are having fun these days"

"'Fun', huh? I guess you are the one having fun"

"Giving an intern new job every time is indeed very fun, hehe"

The Manager sure likes to tease you. Every week, not only he gives you new jobs every week, you must solve said jobs with new concepts as well. If not for the fact that you already learned said concepts, it might not be possible for you to solve those jobs. Those jobs wouldn't be solved if you were an ordinary intern but it seems The Manager doesn't know that, or perhaps doesn't care, or in another words The Manager likes to have fun.

"Well it's about time for you to have fun too, I guess", says The Manager while giving you something.

"A staff? What is this for?"

"A reward for a hardworking intern, I guess. Well let's say that the staff is useful for you to have fun easier"

"Just say that this is useful to help me doing my job"

"Yeah, something like that. If you think you are in a pinch, you can use it to access The Library, you might find things you need there"

Well, what a convenience tool, you think. With this staff, you can access researches you need to help you with your job. You can't thank The Manager enough for this. Of course, you feel that your job will be harder, or in The Manager's words, you will "have more fun".

"Let's talk about job, then. How good are you at decorating things?"

"Is that even a magic intern's job?"

"Well... you need to adapt if you want to keep working here. Let me know if you are ready. Jobs are waiting for you", says The Manager while leaving you.

After The Manager leaves you alone, you spend some time thinking about "the job". 

"I need to be good at decorating things since in order to keep working here, I need to adapt... Ah". You think you know what The Manager means. It's no other than Decorator Pattern and Adapter Pattern. Of course, it will be related to your next jobs. You think but perhaps you need to appreciate The Manager's joke, or at least, attempt to joke. 
## Decorator Pattern
You take a closer look at the staff given to you. It seems to have a set of Magic Skills ready to use. It seems that you can also attach upgrades to these skills to make them stronger. You take a look at the available upgrades and smile. A Fireball that can freeze? Interesting.

You think about the pattern that allows you to decorate objects with other objects: the Decorator Pattern.

The Decorator Pattern is a Structural Design Pattern. That means Decorator Pattern seeks to arrange multiple classes and interfaces into flexible and efficient structure while it gets larger.

So, what does Decorator Pattern try to accomplish? Decorator Pattern tries to let an object to have another behaviour from another object from the same class without affecting behaviour of other objects from same class. How does Decorator Pattern do that? Decorator Pattern solves the problem by using composition and inheritance to some degree. The decorator class inherits the main component class and also has an instance variable to object from said main component class. To add behaviour from said object, you can call the object's method related to the behaviour. Of course, since the decorator class is subclass of the main component class, you can also decorate objects from decorator class. In another words, you can apply recursive decoration.

Well, that's what Decorator Pattern is and how it works. You feel ready to to "have fun" for every job The Manager gives you.

## Decorator Pattern : The Task
You have a selected list of Magic Skills at your disposal, namely `Fireball`, `Magic Missile`, and `Thunderbolt`.

You are also given four Magic Upgrades to use, namely `PowerUpgrade`, `ManacostUpgrade`, `FreezeUpgrade`, and `Lifesteal Upgrade`.

The way you see it, **Upgrades** are also **Skills**. When you want to upgrade the skills, each of those skills are wrapped in a new **Upgrade Skill**, and becomes that new skill, while retaining its old parameters, only changing what's necessary.

When choosing an upgrade, all Skills you have will be upgraded with the selected upgrade. 

Your task is to complete the classes that have yet to be completed.


## Adapter Pattern
While bringing the staff The Manager gave you earlier, you go to The Library again. After you arrive at The Library, you go to find books to learn about the next design pattern : Adapter Pattern.

Adapter Pattern, like Decorator Pattern, is a Structural Design Pattern. Like Decorator Pattern, Adapter Pattern seeks to arrange multiple classes and interfaces into flexible and efficient structure while it gets larger.

So what does Adapter Pattern try to accomplish? Adapter Pattern tries to fit incompatible classes (or interfaces) into an existing classes without changing said incompatible classes.

There are two approaches for Adapter Pattern: class adapter and object adapter. Class adapter utilizes multi-inheritance on its implementation while object adapter use composition on its implementation. You try to learn the object adapter approach and ignore the class adapter approach since you can't use class adapter in Java since Java doesn't support multi-inheritance.

As stated before, object adapter utilizes composition on its implementation. The adapter inherits characteristics from desired class or interface and have an instance variable for object from the incompatible class or interface. The adapter will override the methods from inherited class (or interface) and assign the let the incompatible object (that is object from incompatible class or interface stored in instance variable) do the execution.

Well, that's all about (Object) Adapter Pattern. Now that you understand Adapter Pattern, you go straight to The Manager's room to receive jobs. You are excited to finish the job and, of course, get paid.
## Adapter Pattern : The Task

The manager shows you a system prototype. It is actually a simple single page system. 
It has a magic list that a magic knight can learn. Beside it, there is a simulation of magic knight learning magic and
his current attack style. In the beginning it just no magic, an ordinary human ability. It will change once 
the magic in the list is clicked. 

![homepage](images/Selection_040.png)

"Quite fancy, doesn't it? In fact this system is a working magic simulation. It using this web magic and archive magic. "

The manager explaining the concept behind the system. Something that makes you wonder. The manager clearly said that
this system is working. Then, what do i need to do? Aren't he just flexing just now? 

"Are you familiar with the magic in this list?"

"Yes. They are common magic that i know. " 

"Correct. This is indeed  ordinary magic. But, this is where i need you. "

The manager's word make you more confused. "Whatever, i will just follow his game".

"The supernatural abilities in this world not just magic. Magic is just one of them. Maybe you have heard about grace. Once upon a time
a guardian angel of this world have this kind of power. Or perhaps you know about demon's power? The curse. "

While the manager explaining, you started to get his intention. As far as you understand it, the system simulates a knight learn magic power.
But in fact, this world has more than magic to offers. That means...

"So you want me to make the system can't adapt to power beside magic?"

The manager smiles and clapping his hands. 

"Correct. That's why i like you. "

The manager grabs a book and ask you to look at the page his finger pointing at. 

"This is  a Mage Aspect. Something more powerful than magic, but harder to master. You can say that every average magician in this
world can learn magic just fine. But it's different with Mage Aspect. You need certain factor to be able to master it. 
For example you need a family's aspect. The Mage Aspect are abilities that belongs to certain family. "

The manager pointing at different pages. You see a lot of words that you can't understand. Some part of it are written in language
that you understand, but generally you can't get the meaning of it.

```text
(In a desperate conflict with a ruthless enemy.)

Zwolhi viyantas was fests luor proi
Yuku dalfe swoivo swen'ne yattu vu hyenvi nes
Sho fu bryu praffi stassui tsenva ches
Yen ryus sois nyaddu piyaro shen'nye flu

Prasweno
Troden shes vi hyu vu praviya
Dyu prostes fis hien hesnye ryanmie proshka
Wi swen ryasta krohtz pro'ine shenhye val yaddu
Nyam laika ridu squois trahpa tof

```
  "Can you understand it?"
  
  "No.."
  
  "Ahaha, so can't i"
  
  "....."
  
  The manager suddenly shows a serious expression. He is tapping your shoulder.
  
  "This is a Lost Attribute. A power even greater than Mage Aspect. Only Graces, Curses, and authorities that rivals their power. But i have one sample of it."
  
  You look at the code that manager shows you. 
  
  *Reality manipulation.. You are kidding me*
  
  You get the point of his serious attitude. This power is beyond human understanding. 
  
"Fortunately, i have sample code of Mage Aspect and Lost Attribute. Both of it can't be used in this system right now since it has 
compatibility issue. You can see right here, all magic classes are implementing Magic Interface, while Mage Aspect and Lost Attribute don't. Can you work on that?
Oh please don't edit the Mage Aspect and Lost Attribute classes."

In general you need to create a converter, because you can't edit the classes directly. You suddenly remember pattern that suits the problem here. The Adapter pattern. You accept the job. 
The manager looks relieved. His face shows that he is afraid of something. 

"Good luck then. "

You feel that the manager asking for help. He asked you not to fail this job. You wonder what it is. When you
go outside of the room, you found your answer. [] is there, smiling suspiciously. You understand that this job actually come from [].
What he wants from Mage Aspect and Lost Attribute's power ? You feel it is something inhuman and shouldn't be done. 
But for now, you don't have a choice other than to accept the job.

## The Job

Your  job is to implements Adapter pattern to fix compatibility issue with Mage Aspect and Lost Attribute.
You can't change the original class nor edit other files without TODO command on it. 

Your task is to create two Adapter  to compatible with magic interface. So the list in the system can show it.

You also need to edit `MagicService.java` so the system will work as intended.   

Checklist :

- [ ] Read the tutorial
- [ ] Do Decorator problem set
- [ ] Do Adapter problem set