Singleton Problem Implementation

Lazy

Positives:
- Instantiate the class instance only when it is really needed

Negatives:
- Can cause problem with concurrency if multiple threads can access the getInstance method at the same time, as it can trigger the creation of a new instance multiple times


Eager

Positives:
- Since it is instantiated while the class is loaded into the application by the JVM, it can be used by multiple threads

Negatives:
- It instantiates the instance every time the class is loaded in an application, even if it isn't used at all