package id.ac.ui.cs.tutorial9.factory.service;

import id.ac.ui.cs.tutorial9.factory.core.factory.DuellistFactory;
import id.ac.ui.cs.tutorial9.factory.core.factory.KnightFactory;
import id.ac.ui.cs.tutorial9.factory.core.factory.PaladinFactory;
import id.ac.ui.cs.tutorial9.factory.core.factory.WarlockFactory;
import id.ac.ui.cs.tutorial9.factory.core.magicknight.MagicKnight;
import id.ac.ui.cs.tutorial9.factory.repository.KnightRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class KnightServiceImpl implements KnightService {
    private KnightRepository repo;

    public KnightServiceImpl(KnightRepository repo){
        this.repo = repo;
        initRepo();
    }

    public KnightServiceImpl(){
        this(new KnightRepository());
    }

    // BONUS : Change the implementation of this method so it isn't hard-coded anymore
    // Hint : You might want to:
    // 1. Add service and repository for KnightFactory
    // 2. Incorporate said service into FactoryController (see the FactoryController's implementation)
    // 3. Incorporate said service (or repository, if you prefer) to this class
    // 4. Remove every subclass of MagicKnight (make sure the program still works properly)
    // You also need to modify the view/template so it doesn't hard-code the knight's types there
    public MagicKnight createKnight(String name, KnightFactory type){
        MagicKnight res = new MagicKnight(name, type);
        return repo.add(res);
    }

    public List<MagicKnight> getKnights(){
        return repo.getKnights();
    }

    private void initRepo(){
        createKnight("The Assistant", new WarlockFactory());
        createKnight("Jean-Clovis Arlens XI", new DuellistFactory());
        createKnight("Maple", new PaladinFactory());
    }
}