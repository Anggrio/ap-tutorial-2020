package id.ac.ui.cs.tutorial9.factory.repository;

import id.ac.ui.cs.tutorial9.factory.core.magicknight.MagicKnight;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.ArrayList;

@Repository
public class KnightRepository {
    private List<MagicKnight> list;

    public KnightRepository(){
        this.list = new ArrayList<>();
    }

    public List<MagicKnight> getKnights(){
        return list;
    }

    public MagicKnight add(MagicKnight knight){
        list.add(knight);
        return knight;
    }
}