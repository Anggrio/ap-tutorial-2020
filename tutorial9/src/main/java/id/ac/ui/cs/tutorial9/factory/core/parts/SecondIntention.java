package id.ac.ui.cs.tutorial9.factory.core.parts;

public class SecondIntention implements Aura {
    public String activate(){
        return "Second intention : Allows the knight to make opposing knights to make mistake";
    }
}