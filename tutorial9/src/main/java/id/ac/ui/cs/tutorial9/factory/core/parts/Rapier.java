package id.ac.ui.cs.tutorial9.factory.core.parts;

public class Rapier implements Weapon {
    public String attack(){
        return "Rapier : A weapon that can deliver multiple thrusts in quick succession";
    }
}