package id.ac.ui.cs.tutorial9.factory.core.parts;

public class StoutGuardian implements Spell {
    public String cast(){
        return "Stout Guardian : Allows the knight to survive fatal blow";
    }
}