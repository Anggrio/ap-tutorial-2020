package id.ac.ui.cs.tutorial9.factory.core.parts;

public class Explosion implements Spell {
    public String cast(){
        return "Explosion : A spell that allows the knight to make explosion to a certain area. EKUSUPUROOOOOSHION";
    }
}