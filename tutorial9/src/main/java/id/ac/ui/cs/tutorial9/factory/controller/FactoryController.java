package id.ac.ui.cs.tutorial9.factory.controller;

import id.ac.ui.cs.tutorial9.factory.service.KnightFactoryService;
import id.ac.ui.cs.tutorial9.factory.service.KnightService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FactoryController {

    @Autowired
    private KnightService knightService;

    @Autowired
    private KnightFactoryService knightFactoryService;

    @GetMapping("/knights")
    public String getKnights(Model model){
        model.addAttribute("knights", knightService.getKnights());
        model.addAttribute("types", knightFactoryService.getKnightFactories().values());
        return "factory/knights";
    }

    @PostMapping("/newKnight")
    public String createKnight(HttpServletRequest request){
        String name = request.getParameter("name");
        knightService.createKnight(name, knightFactoryService.getKnightFactory(request.getParameter("type")));
        return "redirect:/knights";
    }
}