package id.ac.ui.cs.tutorial9.singleton.service;

import id.ac.ui.cs.tutorial9.singleton.core.AkashicRecord;
import org.springframework.stereotype.Service;

@Service
public class AkashicRecordService {

    private static AkashicRecord akashicRecord = AkashicRecord.getInstance();

    public void addRecord(String record) {
        akashicRecord.addRecord(record);
    }
    
    public String getRandomRecord() {
        return akashicRecord.getRandomRecord();
    }

}