package id.ac.ui.cs.tutorial9.factory.service;

import id.ac.ui.cs.tutorial9.factory.core.factory.*;
import id.ac.ui.cs.tutorial9.factory.repository.KnightFactoryRepository;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class KnightFactoryServiceImpl implements KnightFactoryService {

    private KnightFactoryRepository knightFactoryRepository;

    public KnightFactoryServiceImpl(KnightFactoryRepository repo) {
        this.knightFactoryRepository = repo;
        initRepo();
    }

    public KnightFactoryServiceImpl() {
        this(new KnightFactoryRepository());
    }

    private void initRepo() {
        knightFactoryRepository.add(new DireFactory());
        knightFactoryRepository.add(new DuellistFactory());
        knightFactoryRepository.add(new PaladinFactory());
        knightFactoryRepository.add(new WarlockFactory());
    }


    @Override
    public Map<String, KnightFactory> getKnightFactories() {
        return knightFactoryRepository.getKnightFactories();
    }

    @Override
    public KnightFactory createKnightFactory(KnightFactory type) {
        knightFactoryRepository.add(type);
        return type;
    }

    @Override
    public KnightFactory getKnightFactory(String name) {
        return knightFactoryRepository.getKnightFactory(name);
    }
}
