package id.ac.ui.cs.tutorial9.factory.core.factory;

import id.ac.ui.cs.tutorial9.factory.core.parts.Affinity;
import id.ac.ui.cs.tutorial9.factory.core.parts.Aura;
import id.ac.ui.cs.tutorial9.factory.core.parts.Spell;
import id.ac.ui.cs.tutorial9.factory.core.parts.Weapon;

public interface KnightFactory {
    Affinity createAffinity();
    Aura createAura();
    Spell createSpell();
    Weapon createWeapon();
    String getName();
}