package id.ac.ui.cs.tutorial9.singleton.core;

import java.util.ArrayList;
import java.util.Random;

public class AkashicRecord {

    private ArrayList<String> records = new ArrayList<>();
    private Random randomizer = new Random();

    private static AkashicRecord instance = new AkashicRecord();

    private AkashicRecord() {}

    public static AkashicRecord getInstance() {
        return instance;
    }

    public String getRandomRecord() {
        if (records.isEmpty()) return "Akashic Record is empty";
        else return this.records.get(randomizer.nextInt(records.size()));
    }

    public void addRecord(String record) {
        this.records.add(record);
    }

}