package id.ac.ui.cs.tutorial9.factory.service;

import id.ac.ui.cs.tutorial9.factory.core.factory.KnightFactory;
import id.ac.ui.cs.tutorial9.factory.core.magicknight.MagicKnight;
import java.util.List;

public interface KnightService {
    List<MagicKnight> getKnights();
    MagicKnight createKnight(String name, KnightFactory type);
}