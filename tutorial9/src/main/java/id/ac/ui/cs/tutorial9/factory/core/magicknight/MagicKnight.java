package id.ac.ui.cs.tutorial9.factory.core.magicknight;

import id.ac.ui.cs.tutorial9.factory.core.factory.KnightFactory;
import id.ac.ui.cs.tutorial9.factory.core.parts.Aura;
import id.ac.ui.cs.tutorial9.factory.core.parts.Spell;
import id.ac.ui.cs.tutorial9.factory.core.parts.Weapon;

public class MagicKnight {
    private String name;
    private Weapon weapon;
    private Spell spell;
    private Aura aura;

    // To do : modify this constructor to accommodate Abstract Factory Pattern.
    // Hint : You might want to have additional parameter
    public MagicKnight(String name, KnightFactory knightFactory) {
        this.name = name;

        // change these parts by using Abstract Factory Pattern
        this.aura = knightFactory.createAura();
        this.spell = knightFactory.createSpell();
        this.weapon = knightFactory.createWeapon();
    }

    public String getName(){
        return name;
    }

    public Aura getAura(){
        return aura;
    }

    public Spell getSpell(){
        return spell;
    }

    public Weapon getWeapon(){
        return weapon;
    }
}