package id.ac.ui.cs.tutorial9.factory.core.factory;

import id.ac.ui.cs.tutorial9.factory.core.parts.*;

// TO DO : Complete the implementation of this factory
// Affinity : Disturbance
// Aura : SecondIntention
// Spell : EnhanceSpeed
// Weapon : Rapier
public class DuellistFactory implements KnightFactory {

    public Affinity createAffinity(){
        return new Disturbance();
    }

    public Aura createAura(){
        return new SecondIntention();
    }

    public Spell createSpell(){
        return new EnhanceSpeed();
    }

    public Weapon createWeapon(){
        return new Rapier();
    }

    public String getName() {
        return "Duellist";
    }
}