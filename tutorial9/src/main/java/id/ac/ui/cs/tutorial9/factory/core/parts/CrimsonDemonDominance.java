package id.ac.ui.cs.tutorial9.factory.core.parts;

public class CrimsonDemonDominance implements Aura {
    public String activate(){
        return "Crimson Demon Dominance : An aura that allows the knight to intimidate opposing knights so they are more susceptible to Explosion";
    }
}