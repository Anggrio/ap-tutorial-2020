package id.ac.ui.cs.tutorial9.factory.repository;

import id.ac.ui.cs.tutorial9.factory.core.factory.KnightFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class KnightFactoryRepository {
    private Map<String, KnightFactory> knightFactoryList;

    public KnightFactoryRepository(){
        this.knightFactoryList = new HashMap<>();
    }

    public Map<String, KnightFactory> getKnightFactories(){
        return knightFactoryList;
    }

    public KnightFactory add(KnightFactory knightFactory){
        knightFactoryList.put(knightFactory.getName(), knightFactory);
        return knightFactory;
    }

    public KnightFactory getKnightFactory(String name) {
        return knightFactoryList.get(name);
    }
}
