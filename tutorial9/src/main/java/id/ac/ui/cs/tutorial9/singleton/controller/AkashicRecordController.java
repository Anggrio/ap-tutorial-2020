package id.ac.ui.cs.tutorial9.singleton.controller;

import id.ac.ui.cs.tutorial9.singleton.service.AkashicRecordService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequestMapping("/akashic-record")
public class AkashicRecordController {

    private final AkashicRecordService akashicRecordService;

    public AkashicRecordController(AkashicRecordService akashicRecordService) {
        this.akashicRecordService = akashicRecordService;
    }

    @GetMapping("/")
    public String akashicRecordHome(Model model) {
        String record = akashicRecordService.getRandomRecord();
        model.addAttribute("record", record);
        return "singleton/akashic-record";
    }

    @PostMapping("/add-record")
    public String addRecord(@RequestParam(value = "record") String record) {
        akashicRecordService.addRecord(record);
        return "redirect:/akashic-record/";
    }
}