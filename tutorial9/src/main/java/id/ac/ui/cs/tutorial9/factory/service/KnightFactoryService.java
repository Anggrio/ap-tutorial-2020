package id.ac.ui.cs.tutorial9.factory.service;

import id.ac.ui.cs.tutorial9.factory.core.factory.KnightFactory;

import java.util.Map;

public interface KnightFactoryService {
    Map<String, KnightFactory> getKnightFactories();
    KnightFactory createKnightFactory(KnightFactory type);
    KnightFactory getKnightFactory(String name);
}
