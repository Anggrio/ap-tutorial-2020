package id.ac.ui.cs.tutorial9.factory.core.parts;

public interface Affinity {
    String getDescription();
}