package fileservice;

import filemanager.Response;

public class ResponseFactory<T> {

    public  Response<T> createSuccessResponse(String message, T content) {
        return createResponse(message, content, 200);
    }

    private Response<T> createResponse(String message, T content, int status) {
        Response<T> response = new Response<>();
        response.setStatus(status);
        response.setMessage(message);
        response.setContent(content);
        return response;
    }

    public Response<T> createFailedResponse(String message, T content) {
        return createResponse(message,content, 500);
    }
}
