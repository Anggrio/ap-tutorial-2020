package main;

import IOUtils.IOUtils;

import java.io.*;


public class Main {

    public static void main(String[] args) throws IOException {
        IOUtils ioUtils = new IOUtils();
        ioUtils.executeMain();
    }
}
