package IOUtils;

import employeeService.GuildEmployeeService;
import guildEmployee.GuildStaff;

import java.io.*;
import java.util.List;

public  class IOUtils {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out));
    private GuildEmployeeService employeeService = new GuildEmployeeService();

    public void executeMain() throws IOException {
        while (true) {
            executeWelcomeMessage();
            String response = reader.readLine();
            executeSelected(response);
        }
    }

    private void executeSelected(String response) throws IOException {
        if (response.equals("1")) {
            findAllReponse();
        } else if (response.equals("2")) {
            addNewResponse();
        } else {
            exitResponse();
        }
    }

    private void addNewResponse() throws IOException {
        writer.write("Enter new member identity: \n");
        writer.write("Name: ");
        writer.flush();
        String name = reader.readLine();
        writer.write("\n");
        writer.write("Position: ");
        writer.flush();
        String position = reader.readLine();
        writer.write("\n");
        employeeService.create(name, position);
        writer.flush();
    }

    private void exitResponse() {
        System.exit(0);
    }

    private void findAllReponse() throws IOException{
        List<GuildStaff> allStaff = employeeService.findAll();
        for (int index =0 ; index < allStaff.size() ; index++) {
            GuildStaff guildStaff = allStaff.get(index);
            writer.write(index+1+"\n");
            writer.write("Name: ");
            writer.write(guildStaff.getName());
            writer.write("\n");
            writer.write("ID: ");
            writer.write(guildStaff.getId());
            writer.write("\n");
            writer.write("Position: ");
            writer.write(guildStaff.getPosition());
            writer.write("\n");
            writer.write("Wage: ");
            writer.write(Integer.toString(guildStaff.getWage()));
            writer.write("\n");
        }
        writer.flush();
    }


    private void executeWelcomeMessage() throws IOException {
        writer.write("Welcome to Guild System\n");
        writer.write("1. List All Guild Employee\n");
        writer.write("2. Add new Employee\n");
        writer.write("3. Exit\n");
        writer.flush();
    }

}
