package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public interface SupportAction extends Action {
	
	String support();
}