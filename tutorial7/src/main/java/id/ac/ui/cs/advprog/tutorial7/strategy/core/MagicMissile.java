package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class MagicMissile implements AttackAction {
	
	private static final String ACTION_NAME = "Magic Missile";

	@Override
	public String getDescription() {
		return "Attack targeted magician by generating moving magical projectile towards them";
	}

	@Override
	public String attack(){
		return "Activate " + ACTION_NAME + ". Targeting hostile magician with magical projectile";
	}
}