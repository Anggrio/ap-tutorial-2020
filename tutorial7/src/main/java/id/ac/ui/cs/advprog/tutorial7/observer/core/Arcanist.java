package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class Arcanist extends Researcher {

        public Arcanist(MagicAssociation magicAssociation) {
                name = "Arcanist";
                this.magicAssociation = magicAssociation;
        }

        @Override
        public void update() {
                getResearchList().add(magicAssociation.getResearch());
        }

}
