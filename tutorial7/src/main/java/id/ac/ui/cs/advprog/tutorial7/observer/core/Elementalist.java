package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class Elementalist extends Researcher {

        public Elementalist(MagicAssociation magicAssociation) {
                name = "Elementalist";
                this.magicAssociation = magicAssociation;
        }

        @Override
        public void update() {
                getResearchList().add(magicAssociation.getResearch());
        }
}
