package id.ac.ui.cs.advprog.tutorial7.strategy.service;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial7.strategy.repository.MagicianRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MagicianServiceImpl  implements MagicianService {

    private MagicianRepository magicianRepo;

    @Autowired
    private BehaviourFactory behaviourFactory;

    public MagicianServiceImpl(MagicianRepository magicianRepo) {
        this.magicianRepo = magicianRepo;
        Magician[] initMages = {
            new AntiMagician("Kerry"),
            new DeathMagician("Chrome"),
            new Enhancer("Shirou"),
            new Spellcaster("Magilou")
        };

        for (Magician magician : initMages) {
            updateMagician(magician);
        }
    }

    public MagicianServiceImpl(){
        this(new MagicianRepository());
    }

    @Override
    public Map<String, Magician> getMagicians(){
        return magicianRepo.getMagicians();
    }

    @Override
    public Magician getMagicianByName(String name){
        return magicianRepo.getMagicianByName(name);
    }

    @Override
    public Magician changeActions(Magician magician, AttackAction attackAction, DefenseAction defenseAction, SupportAction supportAction) {
        magician.setAttackAction(attackAction);
        magician.setDefenseAction(defenseAction);
        magician.setSupportAction(supportAction);
        return magician;
    }

    public Magician updateMagician(Magician  magician) {
        magicianRepo.add(magician);
        return magician;
    }

    @Override
    public Magician changeActions(String magicianName, String attackBehaviour, String defenseBehaviour, String  supportBehaviour) {
        return changeActions(getMagicianByName(magicianName),
                behaviourFactory.setAttackAction(attackBehaviour),
                behaviourFactory.setDefenseAction(defenseBehaviour),
                behaviourFactory.setSupportAction(supportBehaviour));
    }
}