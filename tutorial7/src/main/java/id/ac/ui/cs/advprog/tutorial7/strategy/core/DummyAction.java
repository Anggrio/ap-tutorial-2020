package id.ac.ui.cs.advprog.tutorial7.strategy.core;

/**
* Obviously not a dummy class
* Note : actually it is, you can delete this class after completing every task for Strategy Pattern
*/
public class DummyAction implements AttackAction, DefenseAction, SupportAction {

	private static final String DO_NOTHING = "Do nothing";
	public String getDescription(){
		return DO_NOTHING;
	}

	@Override
	public String attack(){
		return DO_NOTHING;
	}

	@Override
	public String defense(){
		return DO_NOTHING;
	}

	@Override
	public String support(){
		return DO_NOTHING;
	}
}