package id.ac.ui.cs.advprog.tutorial7.strategy.service;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial7.strategy.repository.ActionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActionServiceImpl  implements ActionService {

    private ActionRepository actionRepo;

    public ActionServiceImpl(ActionRepository actionRepo) {
        this.actionRepo = actionRepo;
        actionRepo.addAttackAction(new Harvest());
        actionRepo.addAttackAction(new MagicMissile());
        actionRepo.addDefenseAction(new Barrier());
        actionRepo.addDefenseAction(new Counter());
        actionRepo.addSupportAction(new Enhance());
        actionRepo.addSupportAction(new Regenerate());
    }

    public ActionServiceImpl(){
        this(new ActionRepository());
    }

    @Override
    public List<AttackAction> getAttackActions(){
        return actionRepo.getAttackActions();
    }

    @Override
    public List<DefenseAction> getDefenseActions(){
        return actionRepo.getDefenseActions();
    }

    @Override
    public List<SupportAction> getSupportActions(){
        return actionRepo.getSupportActions();
    }
}