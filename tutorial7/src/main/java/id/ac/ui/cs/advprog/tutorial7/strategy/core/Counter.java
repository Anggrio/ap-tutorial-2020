package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Counter implements DefenseAction {
	
	private static final String ACTION_NAME = "Counter";

	@Override
	public String getDescription(){
		return "A spell that can reflect attacker's offensive spell towards them";
	}

	@Override
	public String defense() {
		return "Activate " + ACTION_NAME + ". A reflective field will reflect damage this magician received";
	}
}