package id.ac.ui.cs.advprog.tutorial7.strategy.service;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.*;
import org.springframework.stereotype.Service;

@Service
public class MagicAssociationBehaviourFactory implements BehaviourFactory {

    @Override
    public AttackAction setAttackAction(String attackAction) {
        if (attackAction.contains("Harvest")) {
            return new Harvest();
        }
        return new  MagicMissile();
    }

    @Override
    public DefenseAction setDefenseAction(String defenseAction) {
        if (defenseAction.contains("Barrier")) {
            return new Barrier();
        }
        return new Counter();
    }

    @Override
    public SupportAction setSupportAction(String supportAction) {
        if (supportAction.contains("Enhance")) {
            return new Enhance();
        }
        return new Regenerate();
    }
}
