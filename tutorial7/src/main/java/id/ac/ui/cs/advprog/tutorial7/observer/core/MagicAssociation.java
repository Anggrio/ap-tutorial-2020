package id.ac.ui.cs.advprog.tutorial7.observer.core;

import java.util.ArrayList;
import java.util.List;

public class MagicAssociation {

        private List<Researcher> researchers = new ArrayList<>();
        private MagicResearch magicResearch;

        public void add(Researcher researcher) {
                researchers.add(researcher);
        }

        public void addResearch(MagicResearch magicResearch) {
                this.magicResearch = magicResearch;
                broadcast();
        }

        public String getResearchType() {
                return magicResearch.getType();
        }

        public MagicResearch getResearch() {
                return magicResearch;
        }

        public List<Researcher> getResearchers() {
                return researchers;
        }

        private void broadcast() {
                for (Researcher researcher : researchers) {
                        if (researcher.getClass().equals(Arcanist.class) && (magicResearch.getType().equals("ArcaneMagic") || magicResearch.getType().equals("UniversalMagic"))) {
                               researcher.update();
                        } else if (researcher.getClass().equals(Elementalist.class) && (magicResearch.getType().equals("ElementalMagic") || magicResearch.getType().equals("UniversalMagic"))) {
                                researcher.update();
                        } else if (researcher.getClass().equals(GrandMagus.class)){
                                researcher.update();
                        }
                }
        }
}
