package id.ac.ui.cs.advprog.tutorial7.strategy.service;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.AttackAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.DefenseAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.SupportAction;

import java.util.List;

public interface ActionService {

    List<AttackAction> getAttackActions();
    List<DefenseAction> getDefenseActions();
    List<SupportAction> getSupportActions();
}