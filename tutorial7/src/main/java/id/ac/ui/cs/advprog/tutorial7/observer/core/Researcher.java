package id.ac.ui.cs.advprog.tutorial7.observer.core;

import java.util.ArrayList;
import java.util.List;

public abstract class Researcher {

        protected MagicAssociation magicAssociation;
        protected String name;
        private List<MagicResearch> magicResearchList = new ArrayList<>();

        public abstract void update();

        public String getName() {
                return name;
        }

        public List<MagicResearch> getResearchList() {
                return magicResearchList;
        }
}
