package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class GrandMagus extends Researcher {

        public GrandMagus(MagicAssociation magicAssociation) {
                name = "GrandMagus";
                this.magicAssociation = magicAssociation;
        }

        @Override
        public void update() {
                getResearchList().add(magicAssociation.getResearch());
        }
}
