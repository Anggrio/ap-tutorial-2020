package id.ac.ui.cs.advprog.tutorial7.observer.controller;

import id.ac.ui.cs.advprog.tutorial7.observer.core.MagicResearch;
import id.ac.ui.cs.advprog.tutorial7.observer.service.MagicAssociationServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ObserverController {

        @Autowired
        private MagicAssociationServiceImpl magicAssociationService;

        @GetMapping("/create-research")
        public String createResearch(Model model){
                model.addAttribute("research", new MagicResearch());
                return "observer/researchForm";
        }

        @PostMapping("/add-research")
        public String addResearch(@ModelAttribute("research") MagicResearch magicResearch) {
                magicAssociationService.addResearch(magicResearch);
                return "redirect:/researcher-list";
        }

        @GetMapping("/researcher-list")
        public String getResearchers(Model model){
                model.addAttribute("researchers", magicAssociationService.getResearchers());
                return "observer/researcherList";
        }
}
