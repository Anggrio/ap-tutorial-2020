package id.ac.ui.cs.advprog.tutorial7.observer.service;

import id.ac.ui.cs.advprog.tutorial7.observer.core.*;
import id.ac.ui.cs.advprog.tutorial7.observer.repository.MagicResearchRepository;
import id.ac.ui.cs.advprog.tutorial7.observer.core.Arcanist;
import id.ac.ui.cs.advprog.tutorial7.observer.core.Elementalist;
import id.ac.ui.cs.advprog.tutorial7.observer.core.GrandMagus;
import id.ac.ui.cs.advprog.tutorial7.observer.core.Researcher;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MagicAssociationServiceImpl implements MagicAssociationService {

        private final MagicResearchRepository researchRepository;
        private final MagicAssociation magicAssociation;
        private final Researcher arcanist;
        private final Researcher elementalist;
        private final Researcher grandMagus;

        public MagicAssociationServiceImpl(MagicResearchRepository researchRepository) {
                this.researchRepository = researchRepository;
                magicAssociation = new MagicAssociation();
                arcanist = new Arcanist(magicAssociation);
                elementalist = new Elementalist(magicAssociation);
                grandMagus = new GrandMagus(magicAssociation);
                magicAssociation.add(arcanist);
                magicAssociation.add(elementalist);
                magicAssociation.add(grandMagus);

        }

        @Override
        public void addResearch(MagicResearch magicResearch) {
                magicAssociation.addResearch(magicResearch);
                researchRepository.save(magicResearch);
        }

        @Override
        public List<Researcher> getResearchers() {
                return magicAssociation.getResearchers();
        }
}
