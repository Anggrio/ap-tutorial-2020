package id.ac.ui.cs.advprog.tutorial7.strategy.service;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.AttackAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.DefenseAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.SupportAction;

public interface BehaviourFactory {

     AttackAction setAttackAction(String attackAction);
     DefenseAction setDefenseAction(String defenseAction);
     SupportAction setSupportAction(String supportAction);
}
