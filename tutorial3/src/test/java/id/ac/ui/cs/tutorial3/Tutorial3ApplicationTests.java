package id.ac.ui.cs.tutorial3;

import id.ac.ui.cs.tutorial3.controller.ManalithController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class Tutorial3ApplicationTests {

    @Autowired
    private ManalithController manalithController;

    @Test
    void contextLoads() {
        assertThat(manalithController).isNotNull();
    }

}
