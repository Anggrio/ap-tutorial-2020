package id.ac.ui.cs.tutorial0;

import id.ac.ui.cs.tutorial0.controller.AdventurerController;
import id.ac.ui.cs.tutorial0.controller.MainController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class Tutorial0ApplicationTests {

    @Autowired
    private AdventurerController adventurerController;

    @Autowired
    private MainController mainController;

    @Test
    void contextLoads() {
        assertThat(adventurerController).isNotNull();
        assertThat(mainController).isNotNull();
    }

}
