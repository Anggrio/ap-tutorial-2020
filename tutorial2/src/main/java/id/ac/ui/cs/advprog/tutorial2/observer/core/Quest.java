package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Quest {
        private String title;
        private String type;

        public void setTitle(String title) {
                this.title = title;
        }

        public void setType(String type) {
                this.type = type;
        }

        public String getTitle() {
                return title;
        }

        public String getType() {
                return type;
        }
}
