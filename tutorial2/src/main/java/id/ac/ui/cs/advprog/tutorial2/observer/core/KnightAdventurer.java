package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class KnightAdventurer extends Adventurer {
    public KnightAdventurer(Guild guild){
        super("Knight", guild);
    }

    @Override
    public void update(){
        this.getQuests().add(this.guild.getQuest());
    }
}
