package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class MysticAdventurer extends Adventurer {
    public MysticAdventurer(Guild guild){
        super("Mystic", guild);
    }

    @Override
    public void update(){
        if (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("E")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
