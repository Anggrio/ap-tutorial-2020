package id.ac.ui.cs.advprog.tutorial2.observer.service;

import id.ac.ui.cs.advprog.tutorial2.observer.core.*;
import id.ac.ui.cs.advprog.tutorial2.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial2.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                this.agileAdventurer = new AgileAdventurer(guild);
                this.knightAdventurer = new KnightAdventurer(guild);
                this.mysticAdventurer = new MysticAdventurer(guild);
        }

        @Override
        public void addQuest(Quest quest) {
                questRepository.save(quest);
                this.guild.broadcastQuest(quest);
        }

        @Override
        public List<Adventurer> getAdventurers() {
                return guild.getAdventurers();
        }

        public Adventurer getAgileAdventurer(){
                return agileAdventurer;
        }

        public Adventurer getKnightAdventurer(){
                return knightAdventurer;
        }

        public Adventurer getMysticAdventurer(){
                return mysticAdventurer;
        }
}
