package id.ac.ui.cs.advprog.tutorial2;

import id.ac.ui.cs.advprog.tutorial2.observer.controller.ObserverController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class Tutorial2ApplicationTests {

    @Autowired
    private ObserverController observerController;

    @Test
    void contextLoads() {
        assertThat(observerController).isNotNull();
    }

}
