package id.ac.ui.cs.tutorial4;

import id.ac.ui.cs.tutorial4.controller.APIController;
import id.ac.ui.cs.tutorial4.controller.AuthController;
import id.ac.ui.cs.tutorial4.controller.HomeController;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class Tutorial4ApplicationTest {

	@Autowired
	private APIController apiController;

	@Autowired
	private AuthController authController;

	@Autowired
	private HomeController homeController;

	@Test
	public void contextLoads() throws Exception {
		assertThat(apiController).isNull();
		assertThat(authController).isNull();
		assertThat(homeController).isNull();
	}

}
