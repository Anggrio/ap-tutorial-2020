package id.ac.ui.cs.tutorial4.dataclass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * KeyPhraseResponseDocument
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyPhrasesResponseDocument {

    @JsonProperty("id")
    String id;

    @JsonProperty("keyPhrases")
    List<String> keyPhrases = Collections.synchronizedList(new ArrayList<String>());

    public KeyPhrasesResponseDocument() {
    }

    public KeyPhrasesResponseDocument(String id, List<String> keyPhrases) {
        this.id = id;
        this.keyPhrases = keyPhrases;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getKeyPhrases() {
        return this.keyPhrases;
    }

    public void setKeyPhrases(List<String> keyPhrases) {
        this.keyPhrases = keyPhrases;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof KeyPhrasesResponseDocument)) {
            return false;
        }
        KeyPhrasesResponseDocument keyPhraseResponseDocument = (KeyPhrasesResponseDocument) o;
        return Objects.equals(id, keyPhraseResponseDocument.id) && Objects.equals(keyPhrases, keyPhraseResponseDocument.keyPhrases);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, keyPhrases);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", keyPhrases='" + getKeyPhrases() + "'" +
            "}";
    }
}