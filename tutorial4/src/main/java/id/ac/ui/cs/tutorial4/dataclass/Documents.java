package id.ac.ui.cs.tutorial4.dataclass;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Documents
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Documents {

    @JsonProperty("documents")
    List<Document> documentsList = new ArrayList<>();

    public Documents() {
    }

    public Documents(List<Document> documents) {
        this.documentsList = documents;
    }

    public List<Document> getDocuments() {
        return this.documentsList;
    }

    public void setDocuments(List<Document> documents) {
        this.documentsList = documents;
    }

    public void addDocument(Document document) {
        document.setId(String.valueOf(documentsList.size() + 1));
        documentsList.add(document);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Documents)) {
            return false;
        }
        Documents document1 = (Documents) o;
        return Objects.equals(this, document1);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(documentsList);
    }

    @Override
    public String toString() {
        return "{" +
            " documents='" + getDocuments() + "'" +
            "}";
    }
}