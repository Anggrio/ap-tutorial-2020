package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CraftServiceImpl implements CraftService {
    private static final String DRAGON_BREATH = "Dragon Breath";
    private static final String VOID_RHAPSODY = "Void Rhapsody";
    private static final String DETERMINATION_SYMPHONY = "Determination Symphony";
    private static final String OPERA_OF_WASTELAND = "Opera of Wasteland";
    private static final String FIRE_BIRD = "FIRE BIRD";

    private String[] itemName = {
            DRAGON_BREATH, VOID_RHAPSODY, DETERMINATION_SYMPHONY,
            OPERA_OF_WASTELAND, FIRE_BIRD
    };

    private  List<CraftItem> allItems = new ArrayList<>();

    @Override
    public CraftItem createItem(String itemName) {
        CraftItem craftItem;
        switch (itemName) {
            case DRAGON_BREATH:
                craftItem = createDragonBreath();
                break;
            case VOID_RHAPSODY:
                craftItem = createVoidRhapsody();
                break;
            case DETERMINATION_SYMPHONY:
                craftItem = createDeterminationSymphony();
                break;
            case OPERA_OF_WASTELAND:
                craftItem = createOperaOfWasteland();
                break;
            case FIRE_BIRD:
                craftItem = createFireBird();
                break;
            default:
                craftItem = createFireBird();
        }
        allItems.add(craftItem);
        return craftItem;
    }

    private CraftItem createDragonBreath() {
        CraftItem dragonBreath = new CraftItem(DRAGON_BREATH);
        CompletableFuture.runAsync(() -> dragonBreath.addRecipes(new SilentField()))
                .thenRunAsync(() -> dragonBreath.addRecipes(new FireCrystal()))
                .thenRun(dragonBreath::composeRecipes);
        return dragonBreath;
    }

    private CraftItem createVoidRhapsody() {
        CraftItem voidRhapsody = new CraftItem(VOID_RHAPSODY);
        CompletableFuture.runAsync(() -> voidRhapsody.addRecipes(new SilentField()))
                .thenRunAsync(() -> voidRhapsody.addRecipes(new PureWaterfall()))
                .thenRun(voidRhapsody::composeRecipes);
        return voidRhapsody;
    }

    private CraftItem createDeterminationSymphony() {
        CraftItem determinationSymphony = new CraftItem(DETERMINATION_SYMPHONY);
        CompletableFuture.runAsync(() -> determinationSymphony.addRecipes(new BlueRoseRainfall()))
                .thenRunAsync(() -> determinationSymphony.addRecipes(new PureWaterfall()))
                .thenRun(determinationSymphony::composeRecipes);
        return determinationSymphony;
    }

    private CraftItem createOperaOfWasteland() {
        CraftItem wasteland = new CraftItem(OPERA_OF_WASTELAND);
        CompletableFuture.runAsync(() -> wasteland.addRecipes(new DeathSword()))
                .thenRunAsync(() -> wasteland.addRecipes(new SilentField()))
                .thenRunAsync(() -> wasteland.addRecipes(new FireCrystal()))
                .thenRun(wasteland::composeRecipes);
        return wasteland;
    }

    private CraftItem createFireBird() {
        CraftItem fireBird =  new CraftItem(FIRE_BIRD);
        CompletableFuture.runAsync(() -> fireBird.addRecipes(new FireCrystal()))
                .thenRunAsync(() -> fireBird.addRecipes(new BirdEggs()))
                .thenRun(fireBird::composeRecipes);
        return fireBird;
    }

    @Override
    public String[] getItemNames() {
        return itemName;
    }

    @Override
    public List<CraftItem> findAll() {
        return allItems;
    }
}
