Lab Tutorial 5

The program in tutorial 5 uses asynchronous implementation because each CraftItem process has several independent process within it.
The original implementation runs each CraftItem process in one synchronous process, so each CraftItem process ended up needing a long
amount of time to finish. The program also originally waits for each process to finish before running the next process, which made adding
each CraftItem also take a long time. 

The solution is to allow each process within the each CraftItem process to run asynchronously so that the next process can be run without
waiting for the previous one to finish. For example, in the original program, each time a new CraftItem is added, the entire program waits 
for it to finish and be displayed before being able to add any new CraftItem. In comparison, the new asynchronous program enables it to add
multiple CraftItems at one time. Each CraftItem process then also runs their processes asynchronously,  so after adding new CraftItem or 
refreshing the page, a process within a Craft may be completed and displayed. The exception is of course the done process which waits for 
the other processes within the CraftItem to finish before being run.

I used the CompletableFuture class to run the processes within the CraftItem asynchronously.

1. First, I instantiated a new CraftItem item according to the function.
1. After that, I used a runAsync() method in a lambda expression to run the first process (addRecipes) on the new item.
2. Then, I used a thenRunAsync() method after the first method to run the second process (addRecipes) asynchronously to the first method on the new item.
If there is a third addRecipe method in a lambda expression, I also used another thenRunAsync() method after the second method on the new item.
3. Finally, I used a thenRun() method in a lambda expression to force the composeRecipe method to be run on the new item after the other processes have finished.
4. The combination of methods are put in each function that handles the different CraftItems (Dragon Breath, Void Rhapsody, etc.) and the original
composeRecipe method is removed from the createItem function.
