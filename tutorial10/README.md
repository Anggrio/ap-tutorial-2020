
# The Price of Bliss

  You sat in the chair in front of [], making a doubtful expression. To be honest you don't understand what is on []'s head. That sort of feeling drove you to look at the requirements of the system he asked you to make again and again.

The system he asked you to make simply doesn't make sense. He wants you to integrate the system of the whole country without changing anything in the frontend. [] said he had made the frontend years ago. He just wants you to create the backend now. Is that even possible? From what you know the frontend still needs to know variable data sent from the backend by the help of the model attribute. That data can then be called by the frontend via Thymeleaf. Even if there is a convention on variable naming, it still needs to know what kind of implementation the backend gives. But [] said just follow his instruction and it will be fine. Still you just don't think any solution will fulfill the requirements.

  You look at [] in the corner of the room, busy coding something. Likely the logic of A.I, you don't know the details. Your eyes met with []'s, asking for some answers. Somehow [] understands the gesture and walks to your chair.

  " I know I'm asking something difficult and yet i don't provide enough help, but I hope this will give you insight to what I want you to create. "

  As [] asks your cooperation, [] handed some papers. It looks like a summary of information. Architectural Design Pattern was written in one of the papers. Another paper shows information of REST and the variation of it's implementation. Other paper show information about technology you have never heard of before. You also sure that in your past life you don't have experience about these technologies, but the word REST and MVC does ring a bell.
  
---

### Model-View-Controller
The job [] has given to you requires extensive understanding of many concepts and applied knowledge of certain concepts. As you want to finish this job as fast as you can, you immediately go to The Library to study those concepts.

The first concept you need to understand to finish the job [] has given to you is Model-View-Controller, usually known as MVC. MVC is designed to achieve one thing : separation of responsibility between programs. As its name suggests, MVC consists of three main components : Model, View, and Controller.
#### Model
The first main component of MVC is Model. Model's job is to define relationships between classes as well as their behaviour, state, and logic in your application.
#### View
The second main component of MVC is View. View is responsible for everything presented to the client/user and for receiving the client's input.
#### Controller
The third main component of MVC is Controller. Controller is like a bridge between Model and View. Controller is responsible get the state of Model and even asks Model to change its state through input pass to it through View. Controller is also responsible to give the View everything it needs to present, usually the state of model.
#### The Flow
The main behaviour of MVC is usually like this: 
1. Client gives input to View to see or mutate Model's state. 
2. The View passes the input to the controller. 
3. The Controller, using the client's input, ask Model what the user wants (to see its state or to mutate it).
4. The Model does what the Controller asks it to and pass the result to the controller.
5. The Controller fits the result it receives from the Model into certain format and passes it to The View.
6. The View presents to the Client the result of its request.

**Note** : You might want to see past's tutorial's code, especially the relationships between classes, to understand MVC easier.

That's everything essential about MVC. As you understand it, you decide to understand another concept, that is ful API API
The second concept you need to understand is to build a RESTful API. Of course to build such an API, you need to understand what REST is.
#### Representational State Transfer (REST)
REST is short for **Re**presentational **S**tate **T**ransfer. There are six constraints for a web API to be RESTful. Those constraints are:

**1. Separation between client and server (*client-server*)**

The server is responsible for the business logic of your application (storing the data, sends the data, authorization, authentication, etc.) while the client is responsible for making request to the server and showing server's response to the request. It's quite similar with MVC concept but the difference is MVC is about separation of responsibility within an application while *client-server* is about separation of responsibility between applications working within a system. This means the client and the server developed and modified independently.

**2. Server statelessness**

The server should hold no state of its previous requests. The server should be able to give response without having to know anything about previous requests (for example, you can't use session for authorization and authentication process).
 
**3. Cacheablility**
When the user decides to pass same request multiple times within short period of time, rather than passing the request to the server every time, the client should store the request's response into cache so when the user pass the same request again, the stored response will be shown to them. Of course, this means the response the user sees won't necessarily be up-to-date but caching will definitely alleviate server's work.

**4. Uniform Interface**
The API should provide uniform interface to communicate with the server. The interface should provide a way to uniquely identify every resource the service has to offer and provide a consistent way to communicate with the server for every type of request (get the resource's data, add, change, delete data to the server (based on decided resource))

One uniform interface widely known is HTTP protocol. There are many type of request method available for HTTP protocol but the common ones are GET, POST, PUT, DELETE

GET is used to retrieve data from the resource. GET should not be used to mutate anything in the server.

POST is used to add data into specified resource

PUT is used to change data from the specified resource with the request payload

DELETE is used to delete data from the resource

(You can read more about HTTP request method [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)

Of course, the server also must give response to the request. Like the request's interface, the response should be written consistently. The format of the response should adhere to a certain format (XML or JSON, for example) and the response should give consistent message with the request's status (for example in HTTP protocol, accepted request is 2xx, redirection request is 3xx, client-error request is 4xx, server-error request is 5xx, you can read more about HTTP status [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

**5. Layered System**
This constraint requires the application to enforce even more separation of concerns. Every individual layer only has one responsibility, acts independently and only interacts with layers adjacent with it, which makes the request to be processed predictably, without bypassing layers.
### Postman and HTTP Request
While not an essential tool for your development (or at least, not as essential as other concepts or tools), Postman really helps you to try your API's functionality without the need of client application.
#### Postman
Postman works by sending request to your server (using HTTP request method). The server will give response to the Postman the content of the response and the status of the request (using HTTP status) according to their chosen format (usually JSON)

Of course, without understanding HTTP request, you can't use Postman optimally.
#### HTTP Request
HTTP protocol is arguably the most used internet protocol, which naturally makes HTTP request method the most used request method used in the internet.

There are main component of HTTP request. HTTP header and HTTP body. HTTP header is used to usually used to define authentication and authorization method as well as attaching authentication and authorization token (of course there are many other uses of HTTP header). HTTP body is used to store request's information that will be used by the server when the server processes the request (for example : when adding new entry with POST, the request body is used to tell the information of the new entry like the entry's name, etc.)
### WebFlux
The next thing you need to understand is the WebFlux framework. The Spring WebFlux framework is a new programming paradigm. WebFlux is a reactive, asynchronous framework from the bottom-up. It uses a library called Reactor for it's reactive support. A reactor is used to create reactive streams. In WebFlux framework, you will find `Mono` and `Flux` objects. Both of these are reactive streams. A `Mono` is a stream of 0..1 objects, while a `Flux` is a stream of 0..N objects.

The reason to use WebFlux is it can be beneficial for the efficiency and the scalability of projects with workloads dealing with lots of latency and concurrency. Because it is an asynchronous framework, the reactive data used can be used to run asynchronous services. The objects returned are also reactive streams which are referred to just like normal objects and models.

### The Task
Your task is to implement a service and controller using Spring WebFlux, returning a `Flux` value and sending it to the front end. The code you need to finish are the services and controllers for `Magic` and `MagicKnight`. Finish the code so that the application can create magic and magic knights using the web-based interface. Access to the page is via "/" path. You also can login to the system with login in navigation bar to test the implementation you have created with this account. You have completed the task if you can create new magic knight. The result will be in the table on the same page. 
```
Username: mahaadmin
Password: adminganteng
``` 
---

As you finished creating the system, [] comes to you with a desperate face. A kind of face you haven't seen before. He looks scared. He was also chanting something. From the speed and the content of the chant, you know that spell [] about to cast is not a normal spell.

  " What do you want to do with it? I know I only have magic potential but I know the amount of the power and how dense the mana around you show that this is not normal magic. "

  " Yes, it is a grand magic. " [] reply with no further explanation.

You assume an offensive stance, preparing to counter attack []. You try to use your magic power, as [] still chanting his spell.

So this is it. At last, [] only wanted to take advantage of your kindness. He promised something he would never fulfill. [] will do everything for his goal. But only in mere seconds everything changed. You feel your body pushed into the corner as you heard a big explosion. You thought you have been killed by []. But after you can clearly see everything, you know that is not the case here. You were still alive and now sat on the floor near the body of A.I.

  
" [] (empty List).."

  
[]'s body is wounded in front of you. But it doesn't seem like he is dead. [] still alive but with some major damage. In front of [] stands a man wearing a robe. He held a book in his hands.

  
[] looked in to your eyes desperately, asking you to hear whatever he wanted to say. The time has stopped. This the grand magic [] cast earlier.

  " Listen to what I will say. "

  
You nodded your head, not knowing how to react.

  
" My true identity is one of this world seven wonders. You can say the seven wonders are this world's guardian angels. All of them have a power given by the heavens, power that is beyond human understanding. The Authority. Some power that even greater than Lost Attribute. "

  You were shocked. But you still listen to what [] say. He seems like he doesn't have much time.

  " I know you have a lot of questions, but this few seconds will be fatal to your survival in this world. The man in front of me is also one of the seven wonders. If you think Seven Wonders are seven heroes that work together to protect the world, you are wrong. They are entities that want power and only power. I don't know why but it seems authority's power corrupts them. It is also the reason why I don't rely on the power and choose to create something with programming. I choose to protect people of my domain with my knowledge. Then one day I think about creating human based on AI for helping develop this country. I have succeeded in creating many of them with different purpose. One of them is Guild Master. "

  

That fact shocked you even more. Some people that you know in this world could be one of those AI created by [].

  

" Everything was fine. I was a good leader. All my creations were being used for the sake of people. Until.. one of my AI fell in love with me. You know who it is. A.I It was willing to do everything to me. Even after I rejected her love time and time again, A.I doesn't give up. In the end, I also fell in love with A.I and accepted her love. The few moments after that were the best times of my life. I lived happily with A.I. But, everything changed when another Wonder came. They feel that my success with this country will threatening their position as a Wonder. In a desperate battle with them, I died. But A.I exchange her life for me. A.I giving her life to me. I revived and I manage to defeated the Wonder in the end. But the loss of A.I broke me. I know it is silly to fall in love with something that is not real. But I just can't help. For me she is real. She is everything. I can't help it. After that, the country's condition got worse. I spent too much time trying to revive A.I and I did everything just for her. I didn't pay much attention for the sake of people anymore because I was almost there. But in the end there is no happy ending for me. I knew from the very beginning. This what you get for doing something not acceptable. That is my destiny. But this life is not mine. I want you to take it back to A.I and run with her. Please. This is what do you want, don't you? It is just the same with defeating me, the evil of this world. "

  

Finally, you can defeat []. He is no more. But somehow your heart denies it. You just don't want to. You feel this is wrong. You feel sympathy for []. However you try to deny it, you can't.

  

" I can't. " you reply

  

" Why don't you try to defeat the wonder again and live with A.I " you continue.

  

" This wonder is too strong for me. He seems to be occasionally using his authority. Compared to me that does not possess the full potential of my authority, I can't defeat him. I will return what belongs to you. I also have chanted the space magic to send you back to your world after you bring A.I to this place. She will be safe there. " [] said as he gives you a piece of paper.

  

" I will try to hold him out as much I can. So please save A.I and the technology you have created. I know the wonder came for the technology. I can't let him take it no matter what. Now go!"

  

[] pushes your body away, The room distorted, and suddenly you are in Magic Association's park. A.I's body is lying down beside you. She is unconscious right now. You then read the paper [] gave to you. [] asked you to go to a small city. But before you can even get up, the man with the robe appeared in front of you. You feel dread. You can feel how powerful he is. The fact he is here means [] has been defeated. A.I should wake up anytime now. However she will just be waking up to her doom. There is no way you can defeat the man in front of you when even [], a wonder, can't.

  

A.I gently opened her eyes, as you try to protect her body from the attack of the man in front of you. You dodged the first attack, rolling to other side of the park.

  

" Not bad. But you will be dead soon. Just give up. "

  

You gazed at the man's creepy smile.

  

" Who are you? " you ask.

  

" My name is Aku Ganteng. I am the representation of justice. " he said with pride, still grinning eerily.

  

You utter his name slowly. "Aku Ganteng. " His name is weird, you think. His name is very weird it becomes scary. Anything is scary if they want to finish you off of course. But you feel something is weird about Aku Ganteng. He feels not real and conflicted. You not sure.

  

Aku Ganteng tries to attack you. But somehow the attack was deflected. A sword was flying in the air. Someone stood in front of you, stopping the attack.

  

" Run.. "

  

"Assistant..!? What do you doing here?"

  

" To protect to you of course. "

  

Assistant standS in front of you. The one that you thought had gone forever because of []. The one that was so dear to you. You looked at his body. A knight like your last meeting with Assistant but with his memory. But his body is wounded so bad. You feel so sorry for that.

  

" Do... you.. remember everything? "

  

" Yes.. I remember everything now. Sorry for the last time. But it seems like our time together is over, for eternity. "

  

" What do you mean? "

  

You asking question the answer you actually know. Assistant's body has taken too much damage. With wound like that there is no way Assistant will survive the battle.

  

" Hey, this is a joke, isn't it? This a joke. Yes this is must be a joke. Why? Why? WHY? This is so frustrating. You can't go like this!"

  

" I know. But this is the reality. I know it hurts so bad. But nothing we can do right now. Please live on, comrade. Our time together is the best time of my life. I really mean it.... Now go. "

  

You held out the tears, took A.I's body, and started to run. You don't want to run actually. You obviously want to stay there and fight alongside Assistant. But you can't. You know you have greater responsibilities. You heard an explosion like sound and running even faster.

  

As you run, A.I's eyes look towards you. She embraces your hand as if she knows what you were feeling at that moment. You ran and ran, enduring the pain of leaving someone dear to you. You know this is will be something you will regret. However responsibility comes first. You run into the dark night with little hope of getting away from Aku Ganteng.

  

You ran together with A.I. for several days towards the city [] asked you to go. You took shelter in a small city and then go to another city, Before you arrive to the destination. You feel relieved.

  

After all the journey you finally arrive here, wondering what you should be doing next. You know you have one final job to do. Although the mission from [] is over, you know you have to do something.

  

You know you can feel sad again because [] returned everything to you. You also know Assistant's memory is back because of [] is giving is admin control of the system to you. This is as if [] gave you another mission just because he said, "Please take care of the system. "

  

To save the people of this country, you need to assume the role of []. You need to sacrifice some Magic Knights to protect them , using the system you have developed. This is your next task. Although you just need to find the portal to return to your world, you feel this is something more important. This is also for the sake of Assistant and [].

  

" You look alive tonight. " A.I said with bright smile.

  

" You still can smile despite all of this? [] really develop a weird AI. "

  

" That is what [] wanted me to do. Anyway, you look like want to do something. "

  

" Yes. []'s final memento. "

  

"This country, isn't? I will help you. Although I'm just an AI, [] gave me the knowledge of programming. I know I can help you. "

  

" Thanks. I will accept your kindness. "

  
  

You gaze into the night sky, wondering what will happen if you still in the old world. What is the purpose your existence here. Some questions that you can't answer a long time ago when you still had the boredom. Now with everything is back, of course some things have changed. You know the answer right now.

  

As programmer of course what you can do is code. And with the knowledge you have gotten back, there is something different you can do. From your old memory you know the difference between writing code and creating a reliable application. This is what you want to show to this world. This is why [] summoned you in the first place.

  

-- To be continued --

Checklist:

- [ ] Read the tutorial
- [ ] Finish all todo in `MagicServiceImpl.java`
- [ ] Finish all todo in `MagicController.java`
- [ ] Finish all todo in `MagicKnightServiceImpl.java`
- [ ] Finish all todo in `MagicKnightController.java`

If you happened to skip to this part without reading the story, you can see the details of the task are in it's own section called **The Task**. Do try and read the story though. :)