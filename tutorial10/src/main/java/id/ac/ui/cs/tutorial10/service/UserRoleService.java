package id.ac.ui.cs.tutorial10.service;

import id.ac.ui.cs.tutorial10.model.UserModel;

public interface UserRoleService {

    UserModel addUser(UserModel user);
    String encrypt(String password);
}
