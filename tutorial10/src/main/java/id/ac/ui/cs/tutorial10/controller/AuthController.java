package id.ac.ui.cs.tutorial10.controller;

import id.ac.ui.cs.tutorial10.model.UserModel;
import id.ac.ui.cs.tutorial10.repository.RoleRepository;
import id.ac.ui.cs.tutorial10.repository.UserRepository;
import id.ac.ui.cs.tutorial10.request.LoginRequest;
import id.ac.ui.cs.tutorial10.response.JwtResponse;
import id.ac.ui.cs.tutorial10.security.jwt.JwtUtils;
import id.ac.ui.cs.tutorial10.service.UserRoleService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    JwtUtils jwtUtils;

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        User userDetails = (User) authentication.getPrincipal();
        Collection<GrantedAuthority> roles = userDetails.getAuthorities();

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getUsername(),
                roles));
    }

    @PostMapping("/signup")
    public UserModel createNewUser(@Valid @RequestBody UserModel userModel) {
        try {
            LOGGER.info(userModel.getUserName());
            LOGGER.info(userModel.getPassword());
            LOGGER.info(userModel.getRoleModel().toString());
            return userRoleService.addUser(userModel);
        } catch (ConstraintViolationException e) {
            return new UserModel();
        }
    }

    @GetMapping("/all")
    public List<UserModel> getAll() {
        List<UserModel> userModels = new ArrayList<>();
        userModels.add(userRepository.findByUserName("mahaadmin"));
        return userModels;
    }
}
