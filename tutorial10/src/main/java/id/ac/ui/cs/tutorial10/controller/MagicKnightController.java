package id.ac.ui.cs.tutorial10.controller;

import id.ac.ui.cs.tutorial10.model.MagicKnightModel;
import id.ac.ui.cs.tutorial10.service.MagicKnightService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/magicknight")
public class MagicKnightController {

    @Autowired
    MagicKnightService magicKnightService;

    @GetMapping("/")
    public Flux<MagicKnightModel> getAllMagicKnights() {
        return magicKnightService.findAll();
    }

    @PostMapping("/create/")
    public ResponseEntity<String> createMagic(@RequestBody MagicKnightModel magicKnightModel) {
        magicKnightService.addKnight(magicKnightModel);
        return ResponseEntity.ok("Added new Magic Knight");
    }

}