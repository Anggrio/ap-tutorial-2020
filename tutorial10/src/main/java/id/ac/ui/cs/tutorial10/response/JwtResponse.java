package id.ac.ui.cs.tutorial10.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter @Setter
public class JwtResponse {

    private String token;
    private String type = "Bearer";
    private String username;
    private Collection<GrantedAuthority> roles;

    public JwtResponse(String accessToken, String username,  Collection<GrantedAuthority> roles) {
        this.token = accessToken;
        this.username = username;
        this.roles = roles;
    }
}
